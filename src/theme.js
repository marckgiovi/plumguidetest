import { createTheme } from "@mui/material";

const theme = createTheme({
  palette: {
    primary: {
      main: "#fdbb30",
    },
    background: {
      main: "#f6ded2",
    },
    black: {
      main: "#000000",
    },
  },
  spacing: [0, 1, 2, 4, 8, 16, 32, 64, 128, 256],
});

export default theme;
