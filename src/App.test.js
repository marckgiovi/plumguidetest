import { render, screen } from '@testing-library/react';
import App from './App';

// Disclaimer: Decided to write most of the layout tests at this level for simplicity
// Ideally they would go as separate unit tests for the layout components

// HEADER

test('renders the Logo correctly', () => {
  render(<App/>);
  const element = screen.getByRole('link', {name: 'logo.svg'});
  expect(element).toBeInTheDocument();
});

test('renders the Menu button correctly', () => {
  render(<App/>);
  const element = screen.getByRole('button', {name: 'menu'});
  expect(element).toBeInTheDocument();
});

test('renders the header \'HOMES\' nav link correctly', () => {
  render(<App/>);
  const element = screen.getByText('HOMES');
  expect(element).toBeInTheDocument();
});

test('renders the header \'HOSTS\' nav link correctly', () => {
  render(<App/>);
  const element = screen.getByText('HOSTS');
  expect(element).toBeInTheDocument();
});

test('renders the header \'Need Help?\' nav link correctly', () => {
  render(<App/>);
  const element = screen.getByText('Need Help?');
  expect(element).toBeInTheDocument();
});

test('renders the header \'Login\' nav link correctly', () => {
  render(<App/>);
  const element = screen.getByText('Login');
  expect(element).toBeInTheDocument();
});

test('renders the Search button correctly', () => {
  render(<App/>);
  const element = screen.getByRole('button', {name: 'search'});
  expect(element).toBeInTheDocument();
});

// CONTENT

test('renders the dates selector correctly', () => {
  render(<App/>);
  const element = screen.getByText('3 Jan 2020 - 28 Feb 2020');
  expect(element).toBeInTheDocument();
});

test('renders the guests selector correctly', () => {
  render(<App/>);
  const element = screen.getByText('1 Guest');
  expect(element).toBeInTheDocument();
});

test('renders the price per night label correctly', () => {
  render(<App/>);
  const element = screen.getByText('£ Per night');
  expect(element).toBeInTheDocument();
});

test('renders the price per night value correctly', () => {
  render(<App/>);
  const element = screen.getByText('345');
  expect(element).toBeInTheDocument();
});

test('renders the total stay price label correctly', () => {
  render(<App/>);
  const element = screen.getByText('£ Total for 54 nights');
  expect(element).toBeInTheDocument();
});

test('renders the total stay price value correctly', () => {
  render(<App/>);
  const element = screen.getByText('18,630');
  expect(element).toBeInTheDocument();
});

test('renders the primary action button correctly', () => {
  render(<App/>);
  const element = screen.getByRole('button', {name: 'Instant Booking'});
  expect(element).toBeInTheDocument();
});

test('renders the property title corectly', () => {
  render(<App/>);
  const element = screen.getByText('Monsieur Didot');
  expect(element).toBeInTheDocument();
});

test('renders the amount of guests correctly', () => {
  render(<App/>);
  const element = screen.getByText('4 people');
  expect(element).toBeInTheDocument();
});

test('renders the amount of bedrooms correctly', () => {
  render(<App/>);
  const element = screen.getByText('2 bedrooms');
  expect(element).toBeInTheDocument();
});

test('renders the amount of bathrooms correctly', () => {
  render(<App/>);
  const element = screen.getByText('2 bathrooms');
  expect(element).toBeInTheDocument();
});

test('renders the amenities description correctly', () => {
  render(<App/>);
  const element = screen.getByText('Private Terrasse');
  expect(element).toBeInTheDocument();
});

test('renders the property description correctly', () => {
  render(<App/>);
  const element = screen.getByText('Peaceful');
  expect(element).toBeInTheDocument();
});

test('renders the location description correctly', () => {
  render(<App/>);
  const element = screen.getByText('Notting Hill, London');
  expect(element).toBeInTheDocument();
});

test('renders the transportation description correctly', () => {
  render(<App/>);
  const element = screen.getByText('Walk 6 mins (Westbourne Park Station)');
  expect(element).toBeInTheDocument();
});

test('renders the property accesibility description correctly', () => {
  render(<App/>);
  const element = screen.getByText('Stairs');
  expect(element).toBeInTheDocument();
});

// In a real project the carousel would be run with some mocked images in order to test the rendering as well
// skipping that here for the sake of simplicity of implementation
test('renders the carousel spinner correctly', () => {
  render(<App/>);
  const element = screen.getByRole('progressbar');
  expect(element).toBeInTheDocument();
});
