import { Box, useTheme } from "@mui/material";
import { flexCenter } from "../common/sharedStyles";

import {
  RangeSelector,
  GuestsSelector,
  PrimaryActionButton,
  PriceInfo,
} from "../baseComponents";

export default function BookingInfo() {
  const theme = useTheme();

  return (
    <Box
      sx={{
        ...flexCenter,
        flexDirection: "column",
        gap: { xs: theme.spacing(5), md: 0 },
        display: { xs: "flex", md: "grid" },
        gridTemplateColumns: { md: "repeat(5, 1fr)" },
        pt: { lg: theme.spacing(5) },
      }}
    >
      <RangeSelector />
      <GuestsSelector />
      <PriceInfo label={"£ Per night"} value={"345"} />
      <PriceInfo label={"£ Total for 54 nights"} value={"18,630"} />
      <PrimaryActionButton>Instant Booking</PrimaryActionButton>
    </Box>
  );
}
