import { AppBar, Box, IconButton, Grid, Container, Link } from "@mui/material";

import {
  Menu as MenuIcon,
  AccountCircle as AccountCircleIcon,
  Search as SearchIcon,
} from "@mui/icons-material";

import { flexCenter, fontSerif } from "../common/sharedStyles";
import { ReactComponent as Logo } from "../logo.svg";

import { NavButton } from "../baseComponents";

export default function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" color="transparent" elevation={0}>
        <Grid
          container
          direction="row"
          spacing={2}
          {...flexCenter}
          sx={{
            root: { boxShadow: "unset" },
            borderBottom: { md: "1px solid #c9c9c9" },
          }}
        >
          <Grid
            item
            xs={3}
            md={4}
            sx={{
              ...flexCenter,
              justifyContent: { xs: "center", md: "space-evenly" },
            }}
          >
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="menu"
              sx={{ position: "relative", bottom: "4px" }}
            >
              <MenuIcon fontSize="large" />
            </IconButton>
            <Box sx={{ ...fontSerif, display: { xs: "none", md: "flex" } }}>
              <NavButton href={"/homes"}>HOMES</NavButton>
              <NavButton href={"/hosts"}>HOSTS</NavButton>
            </Box>
          </Grid>
          <Grid item xs={6} md={4} {...flexCenter}>
            <Container sx={{ width: { xs: "150px" }, p: { md: "8px" } }}>
              <Link href="/" underline="none" sx={{ color: "black.main" }}>
                <Logo />
              </Link>
            </Container>
          </Grid>
          <Grid
            item
            xs={3}
            md={4}
            sx={{
              ...flexCenter,
              justifyContent: { xs: "center", md: "space-evenly" },
            }}
          >
            <Box sx={{ ...fontSerif, display: { xs: "none", md: "flex" } }}>
              <NavButton href={"/help"}>Need Help?</NavButton>
              <NavButton href={"/login"}>Login</NavButton>
            </Box>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="account"
              sx={{ display: { xs: "block", md: "none" } }}
            >
              <AccountCircleIcon fontSize="large" />
            </IconButton>
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="search"
              sx={{ display: { xs: "none", md: "block" } }}
            >
              <SearchIcon fontSize="large" />
            </IconButton>
          </Grid>
        </Grid>
      </AppBar>
    </Box>
  );
}
