import Header from "./Header";
import Carousel from "./Carousel";
import BookingInfo from "./BookingInfo";
import PropertyDetails from "./PropertyDetails";

export { Header, Carousel, BookingInfo, PropertyDetails };
