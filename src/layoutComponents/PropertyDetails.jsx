import { Box, Typography, Icon, Container, useTheme } from "@mui/material";

import {
  LocationOn as LocationOnIcon,
  DirectionsBus as DirectionsBusIcon,
  Stairs as StairsIcon,
} from "@mui/icons-material";

import { fontSerif, fontBold, flexCenter } from "../common/sharedStyles";

export default function PropertyDetails() {
  const theme = useTheme();

  // Creating it as a local style as don't really think it could be reused anywhere else
  const responsiveCopySize = {
    fontSize: { xs: "16px", lg: "22px" }
  };

  return (
    <Box sx={{ position: "relative", bottom: "40px" }}>
      <Typography
        sx={{
          ...fontSerif,
          ...fontBold,
          fontSize: { xs: "40px", md: "48px", lg: "74px" },
        }}
      >
        Monsieur Didot
      </Typography>

      <Box
        sx={{
          display: "flex",
          flexDirection: { xs: "column", md: "column-reverse" },
        }}
      >
        <Container
          sx={{
            ...flexCenter,
            gap: { xs: theme.spacing(4), md: theme.spacing(5) },
            width: { lg: "unset" },
            borderTop: { lg: "1px solid grey" },
            borderBottom: { lg: "1px solid grey" },
            p: { lg: theme.spacing(5) },
          }}
        >
          <Box
            sx={{
              ...flexCenter,
              lineHeight: "30px",
              borderRight: { lg: "1px solid grey" },
              pr: { lg: theme.spacing(5) },
            }}
          >
            <Icon
              size="small"
              edge="start"
              color="inherit"
              aria-label="location"
            >
              <LocationOnIcon fontSize="small" />
            </Icon>
            <Typography sx={responsiveCopySize}>
              Notting Hill, London
            </Typography>
          </Box>

          <Box
            sx={{
              ...flexCenter,
              lineHeight: "30px",
              display: { xs: "none", md: "flex" },
            }}
          >
            <Icon
              size="small"
              edge="start"
              color="inherit"
              aria-label="location"
            >
              <DirectionsBusIcon fontSize="small" />
            </Icon>
            <Typography sx={responsiveCopySize}>
              Walk 6 mins (Westbourne Park Station)
            </Typography>
          </Box>

          <Box
            sx={{
              ...flexCenter,
              lineHeight: "30px",
              display: { xs: "none", md: "flex" },
              borderLeft: { lg: "1px solid grey" },
              pl: { lg: theme.spacing(5) },
            }}
          >
            <Icon
              size="small"
              edge="start"
              color="inherit"
              aria-label="location"
            >
              <StairsIcon fontSize="small" />
            </Icon>
            <Typography sx={responsiveCopySize}>
              Stairs
            </Typography>
          </Box>
        </Container>

        <Container
          sx={{
            ...flexCenter,
            p: theme.spacing(5),
            fontSize: "13px",
            gap: theme.spacing(5),
          }}
        >
          <Typography sx={responsiveCopySize}>
            4 people
          </Typography>
          <Typography sx={responsiveCopySize}>
            2 bedrooms
          </Typography>
          <Typography sx={responsiveCopySize}>
            2 bathrooms
          </Typography>
          <Typography
            sx={{
              ...responsiveCopySize,
              display: { xs: "none", md: "block" }
            }}
          >
            Private Terrasse
          </Typography>
          <Typography
            sx={{
              ...responsiveCopySize,
              display: { xs: "none", md: "block" }
            }}
          >
            Peaceful
          </Typography>
        </Container>
      </Box>
    </Box>
  );
}
