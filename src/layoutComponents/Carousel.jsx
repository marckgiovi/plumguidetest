import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel as ResponsiveCarousel } from "react-responsive-carousel";

export default function Carousel(props) {
  const { baseImageWidth, imageUrls } = props;

  // This would ideally be a prop to provide more flexibility, but deciding to keep it internally for the purpose of this exercise
  // as it makes more sense to keep all the image sizes logic centralised in a single place for simplicity
  const sourceImageRules = [
    { mediaQuery: "only screen and (min-width: 1440px)", baseSize: "1920" },
    { mediaQuery: "only screen and (min-width: 1280px)", baseSize: "1440" },
    { mediaQuery: "only screen and (min-width: 992px)", baseSize: "1280" },
    { mediaQuery: "only screen and (min-width: 768px)", baseSize: "992" },
    { mediaQuery: "only screen and (min-width: 414px)", baseSize: "768" },
    { mediaQuery: "", baseSize: "414" },
  ];

  return (
    <div className="carousel-wrapper">
      <ResponsiveCarousel
        infiniteLoop
        useKeyboardArrows
        autoPlay
        showIndicators={false}
      >
        {imageUrls.map((url, index) => (
          <div key={index}>
            <picture>
              {sourceImageRules.map(({ mediaQuery, baseSize }, index) => (
                <source
                  key={index}
                  media={mediaQuery}
                  data-srcset={`${url}?ar=5:3&q=55&w=${baseSize} 1x, ${url}?ar=5:3&q=55&w=${
                    baseSize * 2
                  } 2x`}
                  srcSet={`${url}?ar=5:3&q=55&w=${baseSize} 1x, ${url}?ar=5:3&q=55&w=${
                    baseSize * 2
                  } 2x`}
                />
              ))}
              <img
                width={"100%"}
                alt={`Carrousel - ${index + 1}`}
                data-srcset={`${url}?ar=5:3&q=55&w=${baseImageWidth} 1x, ${url}?ar=5:3&q=55&w=${
                  baseImageWidth * 2
                } 2x`}
                srcSet={`${url}?ar=5:3&q=55&w=${baseImageWidth} 1x, ${url}?ar=5:3&q=55&w=${
                  baseImageWidth * 2
                } 2x`}
              />
            </picture>
          </div>
        ))}
      </ResponsiveCarousel>
    </div>
  );
}
