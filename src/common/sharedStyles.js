export const fontSerif = {
  fontFamily: "DomaineDisplayNarrow, 'Times New Roman', serif",
};

export const fontSans = {
  fontFamily:
    "HalyardDisplay, -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif",
};

export const fontBold = {
  fontWeight: "bold",
};

export const textCenter = {
  textAlign: "center",
};

export const flexCenter = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};
