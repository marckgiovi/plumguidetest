import { Button, Box } from "@mui/material";

export default function NavButton({ href, children }) {
  return (
    <Box
      sx={{
        "a:hover": {
          borderBottom: "3px solid",
          borderBottomColor: "primary.main",
        },
      }}
    >
      <Button
        href={href}
        sx={{
          color: "black.main",
          borderRadius: 0,
          borderBottom: "3px solid transparent",
          textTransform: "none",
        }}
      >
        {children}
      </Button>
    </Box>
  );
}
