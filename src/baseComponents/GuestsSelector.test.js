import { render, screen } from "@testing-library/react";
import GuestsSelector from "./GuestsSelector";

test("defaults to 1 when initialValue and maxGuests are not provided", () => {
  render(<GuestsSelector />);
  const element = screen.getByText("1 Guest");
  expect(element).toBeInTheDocument();
});

test("renders without error for maxGuests = 1", () => {
  render(<GuestsSelector initialValue={1} maxGuests={1} />);
  const element = screen.getByText("1 Guest");
  expect(element).toBeInTheDocument();
});

test("renders correctly for maxGuests > 1", () => {
  const { getByText } = render(<GuestsSelector initialValue={2} maxGuests={2} />);
  const element = getByText("2 Guests");
  expect(element).toBeInTheDocument();
});