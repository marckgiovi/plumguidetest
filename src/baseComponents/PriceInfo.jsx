import { Container, Typography } from "@mui/material";

import { fontBold } from "../common/sharedStyles";

export default function PriceInfo({ label, value }) {
  return (
    <Container role="contentinfo">
      <Typography>{label}</Typography>
      <Typography sx={fontBold}>{value}</Typography>
    </Container>
  );
}
