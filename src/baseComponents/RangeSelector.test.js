import { render, screen } from "@testing-library/react";
import RangeSelector from "./RangeSelector";

test("renders correctly", () => {
  render(<RangeSelector maxGuests={2} />);
  const element = screen.getByText("3 Jan 2020 - 28 Feb 2020");
  expect(element).toBeInTheDocument();
});
