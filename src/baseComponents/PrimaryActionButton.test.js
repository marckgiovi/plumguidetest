import { render, screen } from "@testing-library/react";
import PrimaryActionButton from "./PrimaryActionButton";

test("renders the internal button", () => {
  render(<PrimaryActionButton />);
  const element = screen.getByRole("button");
  expect(element).toBeInTheDocument();
});

test("renders the button text correctly", () => {
  render(<PrimaryActionButton>Test</PrimaryActionButton>);
  const element = screen.getByText("Test");
  expect(element).toBeInTheDocument();
});
