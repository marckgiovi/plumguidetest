import React from "react";
import { ThemeProvider } from "@mui/material";

import theme from "../theme";
import NavButton from "./NavButton";

export default {
  title: "Base Components/NavButton",
  component: NavButton,
  parameters: {
    layout: "fullscreen",
  },
};

const Template = (args) => (
  <ThemeProvider theme={theme}>
    <NavButton href={args.href}>{args.text}</NavButton>
  </ThemeProvider>
);

export const Default = Template.bind({});
Default.args = {
  text: "Demo",
  href: "/demo"
};
