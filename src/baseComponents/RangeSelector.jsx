import {
  Container,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";

export default function DateSelector() {
  return (
    <Container>
      <FormControl fullWidth>
        <InputLabel id="date-label">From / To</InputLabel>
        <Select
          labelId="date-label"
          id="date-selector"
          value={1}
          label="From / To"
        >
          {/* Disclaimer: This would ideally use a Range Picker in a real scenario, leaving it as select for the sake of simplicity of this exercise */}
          <MenuItem value={1}>3 Jan 2020 - 28 Feb 2020</MenuItem>
        </Select>
      </FormControl>
    </Container>
  );
}
