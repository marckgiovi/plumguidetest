import {
  Container,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";

export default function GuestsSelector({ initialValue = 1, maxGuests = 1 }) {
  return (
    <Container>
      <FormControl fullWidth>
        <InputLabel id="guests-label">For</InputLabel>
        <Select
          labelId="guests-label"
          id="guests-selector"
          value={initialValue}
          label="For"
        >
          {[...Array(maxGuests)].map((_, idx) => (
            <MenuItem key={idx} value={idx + 1}>
              {idx > 0 ? `${idx + 1} Guests` : "1 Guest"}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Container>
  );
}
