import { Button, useTheme } from "@mui/material";

import { fontSans } from "../common/sharedStyles";

export default function PrimaryActionButton({ children }) {
  const theme = useTheme();

  return (
    <Button
      variant="contained"
      sx={{
        ...fontSans,
        backgroundColor: "primary",
        letterSpacing: theme.spacing(2),
        fontSize: "13px",
        m: theme.spacing(5),
        p: theme.spacing(5),
        borderRadius: 0,
      }}
    >
      {children}
    </Button>
  );
}
