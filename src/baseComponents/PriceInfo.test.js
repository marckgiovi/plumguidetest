import { render, screen } from "@testing-library/react";
import PriceInfo from "./PriceInfo";

test("renders without params", () => {
  render(<PriceInfo />);
  const element = screen.getByRole("contentinfo");
  expect(element).toBeInTheDocument();
});

test("renders properly with params", () => {
  render(<PriceInfo label={"test"} value={"123"} />);

  const element = screen.getByRole("contentinfo");
  expect(element).toBeInTheDocument();

  const label = screen.getByText("test");
  expect(label).toBeInTheDocument();

  const value = screen.getByText("123");
  expect(value).toBeInTheDocument();
});
