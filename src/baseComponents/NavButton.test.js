import { render, screen } from "@testing-library/react";
import NavButton from "./NavButton";

test("renders the internal button", () => {
  render(<NavButton />);
  const element = screen.getByRole("button");
  expect(element).toBeInTheDocument();
});

test("renders the button text correctly", () => {
  render(<NavButton>Test</NavButton>);
  const element = screen.getByText("Test");
  expect(element).toBeInTheDocument();
});
