import React from "react";
import { ThemeProvider } from "@mui/material";

import theme from "../theme";
import PrimaryActionButton from "./PrimaryActionButton";

export default {
  title: "Base Components/PrimaryActionButton",
  component: PrimaryActionButton,
  parameters: {
    layout: "fullscreen",
  },
};

const Template = (args) => (
  <ThemeProvider theme={theme}>
    <PrimaryActionButton href={args.href}>{args.text}</PrimaryActionButton>
  </ThemeProvider>
);

export const Default = Template.bind({});
Default.args = {
  text: 'Call To Action Button',
  href: '/demo'
};
