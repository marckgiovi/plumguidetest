import React from "react";
import { ThemeProvider } from "@mui/material";

import theme from "../theme";
import PriceInfo from "./PriceInfo";

export default {
  title: "Base Components/PriceInfo",
  component: PriceInfo,
  parameters: {
    layout: "fullscreen",
  }
};

const Template = (args) => (
  <ThemeProvider theme={theme}>
    <PriceInfo label={args.label} value={args.value} />
  </ThemeProvider>
);

export const Default = Template.bind({});
Default.args = {
  label: "Test label",
  value: "123,45"
};
