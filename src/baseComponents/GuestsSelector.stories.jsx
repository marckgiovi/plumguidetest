import React from "react";
import { ThemeProvider, Box } from "@mui/material";

import theme from "../theme";
import GuestsSelector from "./GuestsSelector";

export default {
  title: "Base Components/GuestsSelector",
  component: GuestsSelector,
  parameters: {
    layout: "fullscreen",
  },
  argTypes: {
    initialValue: {
      options: [1,2,3,4,5,6,7,8,9,10],
      control: { type: 'select' },
    },    
    maxGuests: {
      options: [1,2,3,4,5,6,7,8,9,10],
      control: { type: 'select' },
    },
  }
};

const Template = (args) => (
  <ThemeProvider theme={theme}>
    <Box sx={{ p: "16px" }}>
      <GuestsSelector initialValue={args.initialValue} maxGuests={args.maxGuests} />
    </Box>
  </ThemeProvider>
);

export const Default = Template.bind({});
Default.args = {};
