import React from "react";
import { ThemeProvider, Box } from "@mui/material";

import theme from "../theme";
import RangeSelector from "./RangeSelector";

export default {
  title: "Base Components/RangeSelector",
  component: RangeSelector,
  parameters: {
    layout: "fullscreen",
  },
};

const Template = (args) => (
  <ThemeProvider theme={theme}>
    <Box sx={{ p: "16px" }}>
      <RangeSelector />
    </Box>
  </ThemeProvider>
);

export const Default = Template.bind({});
Default.args = {};
