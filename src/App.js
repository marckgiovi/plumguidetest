import React, { useState, useEffect } from "react";

import {
  ThemeProvider,
  CssBaseline,
  Box,
  CircularProgress,
  Alert,
  Container,
} from "@mui/material";

import { textCenter, flexCenter } from "./common/sharedStyles";

import theme from "./theme";
import {
  Header,
  Carousel,
  PropertyDetails,
  BookingInfo,
} from "./layoutComponents";

function App({ config }) {
  // Handle carousel error state
  const [error, setError] = useState(false);

  // Handle carousel images request state
  const [isLoaded, setIsLoaded] = useState(false);

  // The carousel images collection
  const [carouselImages, setCarouselImages] = useState([]);

  // Fetch carrousel images from endpoint and update the internal app state
  useEffect(() => {
    fetch(config?.carouselImagesApiUrl)
      .then((res) => res.json())
      .then(
        (result) => {
          // Limit to 30
          const imageUrls = result.imageUrls.slice(0, 30);
          setIsLoaded(true);
          setCarouselImages(imageUrls);
        },
        (error) => {
          setIsLoaded(true);
          setError(true);
        }
      );
  }, [config]);

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <CssBaseline />
        <Header />
        <main role="main">
          <Box
            sx={{
              ...textCenter,
              display: { md: "flex" },
              flexDirection: { md: "column-reverse" },
              gap: { md: theme.spacing(6) },
            }}
          >
            <Box
              sx={{
                ...textCenter,
                display: { md: "flex" },
                flexDirection: { md: "column-reverse" },
                gap: { md: theme.spacing(6) },
                backgroundColor: { lg: "background.main" },
                pt: { lg: theme.spacing(8) },
              }}
            >
              {!isLoaded ? (
                <Container sx={{ ...flexCenter, height: theme.spacing(9) }}>
                  <CircularProgress />
                </Container>
              ) : !error ? (
                <Carousel imageUrls={carouselImages} baseImageWidth={800} />
              ) : (
                <Alert severity="error" sx={flexCenter}>
                  Ooops... Something went wrong while loading the carousel!
                </Alert>
              )}
              <PropertyDetails />
            </Box>
            <BookingInfo />
          </Box>
        </main>
        <footer />
      </div>
    </ThemeProvider>
  );
}

export default App;
