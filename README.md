# Opinionated implementation of the Plum Guide interview coding exercise

## Technology stack decisions

### [Typescript](https://www.typescriptlang.org/)

Decided not to use it, even IMHO it would be certainly convenient for any | implementation at a Corporate level, it was overkill to deal with the overhead it generates for the purpose of this implementation, specially having a single developer working on the codebase.

### [Material-UI](https://mui.com/)

After having evaluated different component libraries, decided to go with Material UI because of its convenience for implementation, availability of components, accesibility and responsive support.

### [Emotion](https://emotion.sh/docs/introduction)

Css-in-code library, plays nicely along Material-UI.

### [Storybook](https://storybook.js.org/)

Develop and showcase components in isolation.

### [Prettier](https://prettier.io/)

Convenience code formatter and simple implementation and usage.

### [React Responsive Carousel](http://react-responsive-carousel.js.org/)

Convenient responsive carousel for react, quick implementation and nice support for features.

## Usage

Ensure to get all deps by running `npm install` beforehand, then use one of the following commands.

### Start app

```
    npm start
```

### Run unit tests

```
    npm test
```

### Start Storybook

```
    npm run storybook
```

### Format with Prettier

```
    npm run prettier
```
